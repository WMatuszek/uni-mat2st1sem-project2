#include "TreeSearcher.h"

#include <sstream>
#include <algorithm>

TreeSearcher::TreeSearcher(std::shared_ptr<Tree<Crossroad>> inputTree){
		using namespace std;
		
		this->tree = inputTree;
		this->root = this->tree->getRoot();
		this->curLayer = { this->root }; 	
		this->paths = { { 0 } };  
	
}

unsigned int TreeSearcher::calculatePathValue(std::vector<int>path) {
	auto node = this->root;
	unsigned int valueSum = node->getPayload()->value; // root
	for (auto it = path.begin() + 1; it != path.end(); ++it) {
		node = node->getChild(*it);
		valueSum += node->getPayload()->value;
	}
	return valueSum;
}

std::string TreeSearcher::pathToStringPretty(std::vector<int> path) {
	std::stringstream ss;
	auto node = this->root;
	ss << "| " << node->getPayload()->name; // root
	for (auto it = path.begin() + 1; it != path.end(); ++it) {
		node = node->getChild(*it);
		ss << " <- ";
		ss << node->getPayload()->name;
	}
	ss << " |";
	return ss.str();
}

std::string TreeSearcher::pathToString(std::vector<int> path) {
	std::stringstream ss;
	for (auto it = path.begin(); it != path.end(); ++it) {
		if (it != path.begin())
			ss << " <- ";
		ss << *it;
	}
	return ss.str();
}

unsigned int TreeSearcher::searchLeaf()
{
	using namespace std;

	bool digginTooDeep = false;
	unsigned int layer = 0;

	while (!digginTooDeep) {
		vector<tn_ptr> nextLayer;
		int pathIdx = 0;

		cout << "Searching layer " << layer << endl;

		// Check layer for leaves
		for (tn_ptr node : this->curLayer) {
			if (node->isLeaf()) {
				cout
						<< "Found leaf in current layer, next layer extraction stop"
						<< endl;
				digginTooDeep = true;
				break;
			}
		}
		if (!digginTooDeep) {
			cout << "No leafs on this layer, extracting next" << endl;
			for (tn_ptr node : this->curLayer) {
				// Prepare next layer
				auto children = node->getChildren();
				nextLayer.insert(nextLayer.end(), children.begin(),
						children.end());

				// Construct paths
				auto nodePath = this->paths.at(pathIdx);
				for (unsigned int i = 1; i < node->childCount(); ++i) {
					auto newPath = vector<int>(nodePath);
					newPath.push_back(i);
					this->paths.insert(this->paths.begin() + pathIdx + i, newPath);
				}
				this->paths.at(pathIdx).push_back(0);

				pathIdx += node->childCount(); // Advance path index by number of children
			}

			// Next layer is current on next iteration
			this->curLayer.clear();
			this->curLayer.insert(this->curLayer.begin(), nextLayer.begin(),
					nextLayer.end());
			layer += 1;
		}
	}
	return layer;
}

std::vector<std::vector<int>> TreeSearcher::createLeafPaths(){
	// Extract paths for all leaves
	std::vector<std::vector<int>> leafPaths;
	for (unsigned int i = 0; i < this->curLayer.size(); ++i) {
		if (this->curLayer.at(i)->isLeaf()) {
			std::cout << "Node " << (i + 1) << " is leaf, adding path "
					<< pathToStringPretty(this->paths.at(i)) << " with "
					<< calculatePathValue(this->paths.at(i))
					<< " score as best path candidate"
					<< std::endl;
			leafPaths.push_back(this->paths.at(i));
		}
	}
	return leafPaths;
}


std::vector<int> TreeSearcher::searchTree() {
	using namespace std;

	cout << endl
			<< "--- Begin searching tree for shortest root-leaf path (length = number of visited nodes) with best score "
			<< endl << endl;

	if (this->tree->getRoot()->isLeaf()) {
		cout << "Root is leaf, done" << endl;
		return std::vector<int> { 0 };
	}

	// Iterate over tree layers, stop at first leaf

	unsigned int layer = this->searchLeaf();

	cout << "Searching for all leaves at layer " << layer << endl;

	// Extract paths for all leaves
	vector<vector<int>> leafPaths = this->createLeafPaths();

	cout << "Comparing shortest paths scores..." << endl;

	// Find path with highest score
	auto it =
			max_element(leafPaths.begin(), leafPaths.end(),
						[this](const vector<int>& x, const vector<int>& y)
						{return calculatePathValue(x) < calculatePathValue(y);});

	cout << "Shortest path is " << pathToStringPretty(*it)
			<< " with score of "
			<< calculatePathValue(*it) << endl;

	this->root = this->tree->getRoot();
	this->curLayer = { this->root }; 	
	this->paths = { { 0 } };  

	return *it;
}
