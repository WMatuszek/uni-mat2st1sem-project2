/*
 * TreeNode.h
 *
 *  Created on: 18 cze 2018
 *      Author: Witold
 */

#ifndef TREE_H_
#define TREE_H_

#include <vector>
#include <memory>

#include "TreeNode.h"

/**
 * Simple tree representation, with each node containing payload of class T
 */
template<class T> class Tree {
public:

	/**
	 * Create tree with root element of default value
	 */
	Tree() {
		this->root = std::make_shared<TreeNode<T>>();
	}

	/**
	 * Create tree with root element of given value
	 * @rootVal root node value
	 */
	Tree(std::shared_ptr<T> rootPayload) {
		this->root = std::make_shared<TreeNode<T>>(rootPayload);
	}

	/**
	 * Return root node
	 * @return root node shared_ptr
	 */
	std::shared_ptr<TreeNode<T>> getRoot() const {
		return this->root;
	}

protected:
	/** Tree root */
	std::shared_ptr<TreeNode<T>> root;
};

#endif /* TREE_H_ */
