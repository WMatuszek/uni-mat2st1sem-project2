/*
 * TreeNode.h
 *
 *  Created on: 18 cze 2018
 *      Author: Witold
 */

#ifndef TREENODE_H_
#define TREENODE_H_

#include <vector>
#include <memory>
#include <stdexcept>      // std::out_of_range

/**
 * Simple tree node representation, containing payload T,
 * 1 parent pointer (non-owning), 0-n children pointers
 */
template<class T> class TreeNode: public std::enable_shared_from_this<
		TreeNode<T>> {
public:
	typedef std::shared_ptr<TreeNode<T>> node_ptr;
	typedef std::shared_ptr<TreeNode<T>> parent_ptr; // #TODO analyze why WEAK_PTR to PARENT caused CHILD PAYLOAD to be released (??!?)
	typedef std::shared_ptr<T> payload_ptr;

public:
	/**
	 * Construct root TreeNode object
	 */
	TreeNode() :
			parent(parent_ptr()), payload(nullptr), amRoot(true) {
	}

	/**
	 * Construct root TreeNode object
	 * @param payload node payload shared_ptr<T>
	 */
	TreeNode(payload_ptr payload) :
			parent(parent_ptr()), payload(payload), amRoot(true) {
	}

	/**
	 * Construct TreeNode object
	 * @param p weak_ptr to parent node
	 */
	TreeNode(parent_ptr p) :
			parent(p), payload(nullptr), amRoot(false) {
	}

	/**
	 * Construct TreeNode object
	 * @param p weak_ptr to parent node
	 * @param payload node payload shared_ptr<T>
	 */
	TreeNode(parent_ptr p, payload_ptr payload) :
			parent(p), payload(payload), amRoot(false) {
	}

	~TreeNode() {
	}

	/**
	 * Add child to node (starting with idx = 0)
	 * @param child child shared_ptr
	 */
	void addChild(node_ptr child) {
		this->children.push_back(child);
	}

	/**
	 * Create and add child to node, with given payload
	 * @param payload payload shared_ptr<T>
	 */
	void addChild(payload_ptr payload) {
		this->children.push_back(
				std::make_shared<TreeNode<T>>(
						parent_ptr(this->shared_from_this()), payload));
	}

	/**
	 * Return node parent
	 * @return parent weak_ptr
	 */
	parent_ptr getParent() const {
		return this->parent;
	}

	/**
	 * Return child at idx, or nullptr
	 * @param idx child index
	 * @return child shared_ptr or throw std::out_of_range if index out of bounds
	 */
	node_ptr getChild(unsigned int idx) const {
		if (this->children.size() > idx)
			return this->children[idx];
		else
			throw std::out_of_range("Child index out of range");
	}

	/**
	 * Return children vector
	 * @return children vector
	 */
	std::vector<node_ptr> getChildren() const {
		return this->children;
	}

	/**
	 * Is node the tree root
	 * @return true if is root
	 */
	bool isRoot() const {
		return amRoot;
	}

	/**
	 * Is node a tree leaf
	 * @return true if is leaf
	 */
	bool isLeaf() const {
		return this->children.empty();
	}

	/**
	 * Return number of node children
	 * @return number of children
	 */
	size_t childCount() const {
		return this->children.size();
	}

	/**
	 * Return node value
	 * @return node value
	 */
	payload_ptr getPayload() const {
		return this->payload;
	}

	/**
	 * Set node value
	 * @param val node value to set (range [0, MAX_VALUE])
	 */
	void setPayload(payload_ptr payload) {
		this->payload = payload;
	}

protected:
	/** Node parent */
	const parent_ptr parent;
	/** Node children */
	std::vector<node_ptr> children;
	/** Payload stored by node */
	payload_ptr payload;
	/** Flag for root node */
	const bool amRoot;
};

#endif /* TREENODE_H_ */
