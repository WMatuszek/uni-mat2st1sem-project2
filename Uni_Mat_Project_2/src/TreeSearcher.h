/*
 * TreeNodeTest.h
 *
 *  Created on: 19 cze 2018
 *      Author: Kamil
 */

#ifndef TREESEARCHER_H
#define TREESEARCHER_H

#include <iostream>
#include <cassert>
#include <memory>

#include "Tree.h"
#include "Crossroad.h"

class TreeSearcher {
		
	private:
	typedef std::shared_ptr<TreeNode<Crossroad>> tn_ptr;
	std::shared_ptr<Tree<Crossroad>> tree;					///< pointer to tree
	tn_ptr root;											///< root node of passed tree
	std::vector<tn_ptr> curLayer;							///< vector of layers
	std::vector<std::vector<int>> paths;					///< vector of paths

	public:
	TreeSearcher(std::shared_ptr<Tree<Crossroad>> inputTree);

	/**
	 * Searches Tree with Crossroad payloads for shortest root-leaf paths,
	 * returns path with best score among them
	 * @return vector representing root -> leaf path
	 * 		   as indices of children in subsequent tree layers, starting with 0 for root
	 */
	std::vector<int> searchTree();
	
	private:
	/**
	 * Calculate path score based on node payload values
	 * @param tree the tree
	 * @param path vector of int representing tree path
	 * @return path score
	 */
	unsigned int calculatePathValue(std::vector<int> path);
	
	/**
	 * Convert path vector to string, using node payload names
	 * @param tree the tree
	 * @param path vector of int representing tree path
	 * @return path as string of payload names
	 */
	std::string pathToStringPretty(std::vector<int> path);
	
	/**
	 * Convert path vector to string
	 * @param path vector of int representing tree path
	 * @return path as string
	 */
	std::string pathToString(std::vector<int> path);
	
	/**
	 * Search Leaf on layers of tree
	 * when method finds a leaf, it stops iterating
	 * @return unsigned int - number of first layer with one or more leaves
	 */
	unsigned int searchLeaf();

		/**
	 * Create Paths to leaves from root node for first layer with one or more leaves
	 * @return vector<vector<int>> - vector of paths to leaves
	 */
	std::vector<std::vector<int>> createLeafPaths();
};


#endif
