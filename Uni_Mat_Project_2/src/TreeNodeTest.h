/*
 * TreeNodeTest.h
 *
 *  Created on: 19 cze 2018
 *      Author: Witold
 */

#ifndef TREENODETEST_H_
#define TREENODETEST_H_

#include <iostream>
#include <cassert>
#include <sstream>
#include <algorithm>

#include "TreeNode.h"
#include "Crossroad.h"


void TEST_TreeNode_simple() {
	using namespace std;

	cout << "TEST_TreeNode_simple start" << endl;

	auto root = make_shared<TreeNode<Crossroad>>(
			make_shared<Crossroad>(5, "DEST"));

	assert(root->isRoot() == true);
	assert(root->isLeaf() == true);

	root->addChild(make_shared<Crossroad>(1, "L1_1"));
	root->addChild(make_shared<Crossroad>(2, "L1_2"));
	root->addChild(make_shared<Crossroad>(3, "L1_3"));

	auto child_1 = root->getChild(0);
	child_1->addChild(make_shared<Crossroad>(4, "L2_1"));

	assert(root->isLeaf() == false);
	assert(root->childCount() == 3);

	assert(child_1->isLeaf() == false);
	assert(child_1->isRoot() == false);
	assert(child_1->getPayload()->value == 1);
	assert(child_1->getChild(0)->isLeaf() == true);
	assert(child_1->getChild(0)->getPayload()->value == 4);

//	assert(child_1->getParent().lock()->isRoot() == true);
//	assert(child_1->getParent().lock()->getPayload()->value == 5);

	auto child_2 = root->getChild(1);

	assert(child_2->isLeaf() == true);
	assert(child_2->isRoot() == false);
	assert(child_2->getPayload()->value == 2);

//	assert(child_2->getParent().lock()->isRoot() == true);
//	assert(child_2->getParent().lock()->getPayload()->value == 5);

	cout << "TEST_TreeNode_simple PASSED" << endl;
}

#endif /* TREENODETEST_H_ */
