//============================================================================
// Name        : Uni_Mat_Project_2.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cassert>
#include <memory>
#include <utility>
#include <algorithm>
#include <sstream>

#include <fstream>

#include "Tree.h"
#include "TreeNode.h"
#include "Crossroad.h"
#include "TreeSearcher.h"

#include "TreeNodeTest.h"

/**
 * Fills tree with example nodes
 * 				      r(5)
 * 			   /       \      \
 * 			c1(1)     c2(2)   c3(3)
 * 		   /      \      \        \
 * 		  c11(1) c12(2)  c21(3)   c31(4)
 * 		   |       |
 * 		 c111(1) c121(2)
 */
void exampleFill(std::shared_ptr<Tree<Crossroad>> &tree) {
	using namespace std;
	auto root = tree->getRoot();
 
	root->setPayload(make_shared<Crossroad>(5, "TOP"));

	root->addChild(make_shared<Crossroad>(1, "Left"));
	root->addChild(make_shared<Crossroad>(2, "Middle"));
	root->addChild(make_shared<Crossroad>(3, "Right"));

	root->getChild(0)->addChild(make_shared<Crossroad>(1, "Left_C1"));
	root->getChild(0)->addChild(make_shared<Crossroad>(2, "Left_C2"));

	root->getChild(1)->addChild(make_shared<Crossroad>(3, "Middle_C1"));

	root->getChild(2)->addChild(make_shared<Crossroad>(4, "Right_C1"));

	root->getChild(0)->getChild(0)->addChild(
			make_shared<Crossroad>(1, "Left_C1_C1"));
	root->getChild(0)->getChild(1)->addChild(
			make_shared<Crossroad>(2, "Left_C1_C2"));
}

/**
* Fills tree with example nodes
* 																		r(5)
* 							   /						  /				 |							 \					\
* 							 c1(4)						c2(2)			c3(3)						c4(1)				c5(5)
* 			  /				  |		     \			   /     \			 |			  /		   /      |		\			  |
*		    c11(3)			c12(5)		c13(1)	   c21(2)   c22(3)	   c31(1)		c41(1)	c42(5)  c43(3)  c44(2)		c51(1)
* 	   /      |	     \					  |											/    \								  |
* 	c111(5) c112(3) c113(1)			    c131(4)								 c411(4)  c412(2)							c511(2)
*/
void exampleFill2(std::shared_ptr<Tree<Crossroad>> &tree) {
	using namespace std;
	auto root = tree->getRoot();

	root->setPayload(make_shared<Crossroad>(5, "TOP"));

	root->addChild(make_shared<Crossroad>(4, "Left"));
	root->addChild(make_shared<Crossroad>(2, "Middle Left"));
	root->addChild(make_shared<Crossroad>(3, "Middle"));
	root->addChild(make_shared<Crossroad>(1, "Middle Right"));
	root->addChild(make_shared<Crossroad>(5, "Right"));

	root->getChild(0)->addChild(make_shared<Crossroad>(3, "Left C1"));
	root->getChild(0)->addChild(make_shared<Crossroad>(5, "Left C2"));
	root->getChild(0)->addChild(make_shared<Crossroad>(1, "Left C3"));

	root->getChild(1)->addChild(make_shared<Crossroad>(2, "Middle Left C1"));
	root->getChild(1)->addChild(make_shared<Crossroad>(3, "Middle Left C2"));
	
	root->getChild(2)->addChild(make_shared<Crossroad>(1, "Middle C1"));

	root->getChild(3)->addChild(make_shared<Crossroad>(1, "Middle Right C1"));
	root->getChild(3)->addChild(make_shared<Crossroad>(5, "Middle Right C2"));
	root->getChild(3)->addChild(make_shared<Crossroad>(3, "Middle Right C3"));
	root->getChild(3)->addChild(make_shared<Crossroad>(2, "Middle Right C4"));

	root->getChild(4)->addChild(make_shared<Crossroad>(1, "Right C1"));

	root->getChild(0)->getChild(0)->addChild(make_shared<Crossroad>(5, "Left C1 C1"));
	root->getChild(0)->getChild(0)->addChild(make_shared<Crossroad>(3, "Left C1 C2"));
	root->getChild(0)->getChild(0)->addChild(make_shared<Crossroad>(1, "Left C1 C3"));

	root->getChild(0)->getChild(2)->addChild(make_shared<Crossroad>(4, "Middle C1 C1"));

	root->getChild(3)->getChild(0)->addChild(make_shared<Crossroad>(4, "Middle Right C1 C1"));
	root->getChild(3)->getChild(0)->addChild(make_shared<Crossroad>(2, "Middle Right C1 C2"));

	root->getChild(4)->getChild(0)->addChild(make_shared<Crossroad>(2, "Right C5 C1"));
}

/**
 * Populates tree based on input file
 * Each file line is of format
 * [0 to n LEAF NUMBERS IN SUBSEQUENT LAYERS] {NODE_WEIGHT} {NODE_NAME}
 * Examples:
 * SomeName 5  		= add node of weight = 5, name = SomeName at layer 0 (to root)
 * 1 SomeName 5 	= add node of weight = 5, name = SomeName at layer 1, node 1 (gets child 1 of root, adds)
 * 1 2 SomeName 5 	= add node of weight = 5, name = SomeName at layer 2, node 2 (gets child 2 of child 1 of root, adds)
 * ADD NODES MUST BE ADDED MANUALLY, SPECIFYING NODE PATH WITHOUT PREVIOUSLY CREATING IT WILL BREAK PARSER
 * CHILDREN ARE ADDED AT SUBSEQUENT INDICES - FIRST ADDED WILL BE 1, SECOND - 2 ETC.
 * @param filename file to read from
 * @return populated tree shared_ptr
 */
void readTreeFromFile(
		std::shared_ptr<Tree<Crossroad>> &tree,
		std::string filename) {
	const int MIN_TOKENS = 2;
	const char TOKEN_DELIM = ' ';

	auto root = tree->getRoot();
	root->setPayload(std::make_shared<Crossroad>(5, "TOP"));

	std::ifstream filestream(filename);
	if (filestream.good() == false) {
		std::cout << "Error opening file" << std::endl;
		exit(0);
	}

	int lineNum = 1;
	for (std::string line; std::getline(filestream, line); ++lineNum) {
		std::istringstream iss(line);

		// Split line into tokens, last one is weight
		std::vector<std::string> tokensStr;
		std::string token;
		while (std::getline(iss, token, TOKEN_DELIM)) {
			tokensStr.push_back(token);
		}

		if (tokensStr.size() < MIN_TOKENS) {
			std::cout << "Line " << lineNum
					<< ": Found line with not enough tokens, skipping"
					<< std::endl;
			continue;
		}

		std::string nodeName = *(tokensStr.end() - 2); // end - 2 token = node name
		int nodeVal = 0;
		try {
			nodeVal = std::stoi(*(tokensStr.end() - 1)); // end - 1 token = node value
		} catch (std::exception &ex) {
			std::cout << "Line " << lineNum
					<< ": wrong number format, cannot convert token: "
					<< *(tokensStr.end() - 1) << std::endl;
			exit(0);
		}

		// Convert all but last token to int
		std::vector<int> tokensInt;
		for (auto it = tokensStr.begin(); it < tokensStr.end() - 2; ++it) {
			try {
				tokensInt.push_back(std::stoi(*it));
			} catch (std::exception &ex) {
				std::cout << "Line " << lineNum
						<< ": wrong number format, cannot convert token: "
						<< *it << std::endl;
				exit(0);
			}
		}

		// Add child to root->node(x1)->...->node(xn)
		auto node = root;
		for (auto it = tokensInt.begin(); it < tokensInt.end(); ++it) {
			if (node->isLeaf()) {
				std::cout << "Line " << lineNum
						<< ": Leaf found too early, cannot add node, first add upper layer nodes"
						<< std::endl;
			} else {
				int idx = (*it) - 1;
				node = node->getChild(idx); // Text file indexing from 1
			}
		}


		std::cout << "Line " << lineNum << ": Adding node " << nodeName
				<< " with weight " << nodeVal << " at layer "
				<< tokensInt.size() + 1 << std::endl;
		std::shared_ptr<Crossroad> payload = std::make_shared<Crossroad>(
				nodeVal, nodeName);
		node->addChild(payload);
	}
}

int main(int argc, char* argv[]) {

	auto tree = std::make_shared<Tree<Crossroad>>();

	if (argc < 2) {
		std::cerr << "Usage: " << argv[0] << " [TREE_CONFIG_FILE_PATH]"
				<< std::endl;
		return 1;
	}

	readTreeFromFile(tree, argv[1]);

	TreeSearcher searcher(tree);
	searcher.searchTree();

	std::cout << "End" << std::endl;

	return 1;
}

