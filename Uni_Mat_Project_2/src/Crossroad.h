/*
 * Crossroad.h
 *
 *  Created on: 20 cze 2018
 *      Author: Witold
 */

#ifndef CROSSROAD_H_
#define CROSSROAD_H_

#include <string>

class Crossroad {
public:

	Crossroad(unsigned int value, std::string name) :
			value(value), name(name) {
	}

	~Crossroad() {
		std::cout << "~Crossroad()" << std::endl;
	}

	unsigned int value;
	std::string name;
};



#endif /* CROSSROAD_H_ */
