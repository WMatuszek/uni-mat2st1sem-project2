Projekt zawiera prosty makefile, budowa� nale�y w konsoli.

Format danych �r�d�owych:
{�cie�ka do punktu wstawienia nowego w�z�a drzewa} {nazwa} {waga}
gdzie �cie�ka podana jest jako indeksy, oddzielone spacjami, od 1, potomk�w wez��w w kolejnych warstwach drzewa.
Kolejno�� dodawania potomk�w decyduje o ich indeksach, pocz�wszy od 1.
Przed dodaniem w�z�a nale�y upewni� sie, �e dodano niezb�dne w�z�y na wy�szych warstwach drzewa.
Waga korzenia ustawiana jest zawsze na 5. Nie ograniczono dopuszczalnych wprowadzanych wag, brak gwarancji dla ujemnych.

Przyk�ad
LVL1 5 		Dodanie w�z�a LVL1 o wadze 5 jako potomek korzenia
2 LVL2 5 		Dodanie w�z�a LVL2 o wadze 5 jako potomek DRUGIEGO potomka korzenia
2 1 LVL3_1 5 	Dodanie w�z�a LVL3_1 o wadze 5 jako (pierwszy) potomek PIERWSZEGO potomka DRUGIEGO potomka korzenia
2 1 LVL3_2 3 	Dodanie w�z�a LVL3_2 o wadze 5 jako (drugi) potomek PIERWSZEGO potomka DRUGIEGO potomka korzenia 

